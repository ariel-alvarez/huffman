#include <iostream>
#include <stdexcept>
#include "CharBag.h"

using namespace std;

/**
 * CharBagStr.repeticiones   ==> Cantidad de repeticiones de cada caracter
 * CharBagStr.caracteres     ==> Caracteres que hayan aparecido al menos una vez
 * CharBagStr.siguienteLibre ==> Siguiente posición libre de @caracteres
 **/
 /*
struct CharBagStr {
	unsigned int* repeticiones;
	unsigned int siguienteLibre;
	unsigned char* caracteres;
};
*/
/**
 * Retorna un CharBag vacío, con capacidad para almacenar n chars.
 *
 * @param longitud El valor máximo que puede alcanzar una representación
 **/
CharBag emptyCharBag(int longitud) {
	CharBag nuevoCharBag = new CharBagStr;
	nuevoCharBag->repeticiones = new unsigned int[longitud];
	nuevoCharBag->siguienteLibre = 0;
	nuevoCharBag->caracteres = new unsigned char[longitud];

	//Se inicializa en cero la cantidad de repeticiones para cada espacio
	for (int i = 0; i < longitud; i++){
		nuevoCharBag->repeticiones[i] = 0;
	}

	return nuevoCharBag;
}

/**
 * Libera la memoria utilizada por el CharBag charBag.
 **/
void deleteCharBag(CharBag& charBag) {
	delete charBag->repeticiones;
	delete charBag;
}

/**
 * Retorna un CharBag vacío, con capacidad para almacenar n chars.
 *
 * @param charBag El CharBag al cual se le agregará el caracter c
 * @param c El caracter a agregar
 **/
void add(CharBag& charBag, unsigned char c) {
	//Si es la primera vez que aparece el caracter c, se agrega a @caracteres
	if (charBag->repeticiones[c] == 0) {
		charBag->caracteres[charBag->siguienteLibre++] = c;
	}

  //Se acumula la nueva repetición del caracter c
	charBag->repeticiones[c]++;
}

/**
 * Retorna la cantidad de ocurrencias del caracter c en el CharBag.
 *
 * @param charBag El CharBag a observar
 * @param c El caracter del cual se quiere conocer sus repeticiones
 **/
int get(CharBag& charBag, unsigned char c) { return charBag->repeticiones[c]; }


/******************************************************************************/
/********************************* ITERADOR ***********************************/
/******************************************************************************/

/**
 * CharBagIteratorStr.charBag   ==> CharBag a ser iterado
 * CharBagIteratorStr.posActual ==> Elemento por el que se está iterando en un
 *                                  momento dado
 **/
struct CharBagIteratorStr {
	CharBag charBag;
	unsigned posActual;
};

/**
 * Construye un iterador a un CharBag apuntando al primer elemento definido.
 *
 * @param charBag El CharBag a ser iterado
 **/
CharBagIterator iterate(CharBag charBag) {
	CharBagIterator charBagIterator = new CharBagIteratorStr;
	charBagIterator->charBag = charBag;
	charBagIterator->posActual = 0;

	return charBagIterator;
}

/**
 * Libera la memoria correspondiente al iterador.
 **/
void deleteCharBagIterator(CharBagIterator& it) {
	delete it;
}

/**
 * Indica si el iterador apunta a un element válido.
 *
 * @param it Iterador a chequear
 **/
bool valid(CharBagIterator it) {
	return it->posActual < it->charBag->siguienteLibre;
}

/**
 * Avanza al siguiente elemento definido en el CharBag
 * (aquellos con ocurrencias mayor o igual a cero)
 * PRE: valid(it)
 * Complejidad: O(1), NOTA: Esto debe ser independiente del tamaño máximo del CharBag
 **/
void next(CharBagIterator it) {
	if(!valid(it))
		throw invalid_argument( "el iterador se encuentra en estado inválido" );
	it->posActual++;
}

/**
 * Retorna el caracter actualmente apuntado por el iterador.
 * PRE: valid(it)
 * Complejidad: O(1)
 **/
char currentChar(CharBagIterator it) {
	return it->charBag->caracteres[it->posActual];
}

/**
 * Retorna la cantidad de ocurrencias del caracter actualmente apuntado por el iterador.
 * PRE: valid(it)
 * Complejidad: O(1)
 **/
int currentCount(CharBagIterator it) {
	return get(it->charBag, currentChar(it));
}

//PRE: el caracter a eliminar ya tiene cero ocurrencias
void removerUnCaracterDeLasApariciones(CharBagIterator &it){
  int pos = it->posActual;

  while(pos < it->charBag->siguienteLibre){
    it->charBag->caracteres[pos] = it->charBag->caracteres[++pos];
  }

  it->charBag->siguienteLibre = pos-1;
}

//DEFENSA
void removeCurrent(CharBagIterator &it){
  if( -- it->charBag->repeticiones[currentChar(it)] == 0 ){
    removerUnCaracterDeLasApariciones(it);
  }
}

void remove(CharBag &charBag, unsigned char c){
  if(charBag->repeticiones[c] == 1){

      CharBagIterator iterador = iterate(charBag);
      while(valid(iterador)){
        if(currentChar(iterador) == c){
          removeCurrent(iterador);
          break;
        }
        next(iterador);
      }

  } else {
    charBag->repeticiones[c]--;
  }
}
