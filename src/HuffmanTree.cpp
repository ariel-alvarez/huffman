#include <cstddef>
#include "HuffmanTree.h"

/******************************************************************************/
/******************************** INTERNAL ************************************/
/******************************************************************************/

//El caracter en sí es irrelevante, se setea sólo para que no quede basura
#define DEFAULT_CHAR '*'
#define DERECHA true
#define IZQUIERDA false

struct HuffmanTreeStr {
  char letra;
  int peso;
  HuffmanTree izq;
  HuffmanTree der;

    //Un poco de syntax sugar para el contructor
    HuffmanTreeStr(char c, int p, HuffmanTree izq, HuffmanTree der):
      letra(c),
      peso(p),
      izq(izq),
      der(der)
    {}
};

/**
 * Constructor para un nodo
 **/
inline HuffmanTree mkNode(char c, int peso, HuffmanTree izq, HuffmanTree der){
  return new HuffmanTreeStr(c, peso, izq, der);
}

/**
 * Constructor para una hoja
 **/
inline HuffmanTree mkLeaf(char c, int p){ return mkNode(c, p, NULL, NULL); }

inline bool isLeaf(HuffmanTree t) { return t->izq == NULL && t->der == NULL; }

//Se agrega la firma para poder usar walkNode en addCaseToTable
void walkNode(ZipTable& zipTable, HuffmanTree nextNode, BitChain bitChain, bool esDerecha);

/**
 * El bit chain va visitando cada nodo, recordando el trayecto que hizo al
 * llegar a él. Al avanzar en una dirección u otra se le agrega IZQUIERDA o
 * DERECHA, y al volver atrás en el camino recursivo, se le quita ese bit.
 **/
void addCaseToTable(ZipTable& zipTable, HuffmanTree t, BitChain bitChain) {
  if (isLeaf(t))
  //Caso Base. Si llegué a una hoja, ya tengo el recorrido que me determina
  //la codificación del caracter de esa hoja, con lo cual la agrego a la
  //zipTable.
  {
    add(zipTable, t->letra, bitChain);
  } else
  //El nodo t no es una hoja, recorro sus hijos a derecha e izquierda
  {
    //Recorro la rama de la derecha
    walkNode(zipTable, t->der, bitChain, DERECHA);
    //Recorro la rama de la izquierda
    walkNode(zipTable, t->izq, bitChain, IZQUIERDA);
  }
}

/**
 * Es el encargado de realizar el paso recursivo, dirigiéndose hacia la rama
 * DERECHA o IZQUIERDA según se le indique
 **/
inline void walkNode(ZipTable& zipTable, HuffmanTree nextNode, BitChain bitChain, bool esDerecha){
  //Agrego al bitChain el bit que corresponde al camino (izquierda o derecha)
  append(bitChain, esDerecha);
  //Llamada recursiva. Para seguir el camino o bien agregar el caso si es una hoja.
  addCaseToTable(zipTable, nextNode, bitChain);
  //Habiendo agregado el caso, doy un paso atrás en el recorrido del árbol
  remove(bitChain);
}

/******************************************************************************/
/******************************** INTERFACE ***********************************/
/******************************************************************************/

/**
 * Construye una hoja de un árbold de Huffman a partir de un caracter y un peso.
 * Complejidad: O(1)
 **/
HuffmanTree leaf(char caracter, int peso){
  return mkLeaf(caracter, peso);
}

/**
 * Construye un nodo binario de un árbol de Huffman a partir de dos hijos.
 * Complejidad: O(1)
 **/
HuffmanTree binary(HuffmanTree a, HuffmanTree b){
  return mkNode(DEFAULT_CHAR, weight(a) + weight(b), a, b);
}

/**
 * Libera toda la memoria utilizada por un árbol de Huffman.
 **/
void deleteHuffmanTree(HuffmanTree& t){
  if (t != NULL){
    deleteHuffmanTree(t->izq);
    deleteHuffmanTree(t->der);
    delete t;
  }
}

/**
 * Retorna el peso de un árbol de Huffman.
 * Cmplejidad: O(1)
 **/
int weight(HuffmanTree t){ return t->peso; }

/**
 * Construye la tabla de compresión asociada al árbol de Huffman.
 **/
ZipTable buildTable(HuffmanTree t) {
  //Se construyen las estructuras
	ZipTable zipTable = emptyZipTable();
	BitChain bitChain = emptyBitChain();

  //Se recorre el árbol agregando cada bitChain a la tabla al llegar a cada hoja
	addCaseToTable(zipTable, t, bitChain);

  //Se libera el bitChain
	deleteBitChain(bitChain);

	return zipTable;
}
