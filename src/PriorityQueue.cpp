#include "PriorityQueue.h"
#include <iostream>

#define MAX_SIZE 256

/**
 * @size cantidad de elementos
 * @elements array de nodos
 *
 * HuffmanTree es siempre completo.
 * Se guardan los nodos de forma consecutiva en un array
 *
 **/
struct PriorityQueueStr {
	int size;
	HuffmanTree* elements;
};

inline void inicializarPriorityQueue(PriorityQueue priorityQueue){
	for (int i = 0; i < MAX_SIZE + 1; i++) priorityQueue->elements[i] = NULL;
}

inline bool tieneMayorPrioridad(HuffmanTree a, HuffmanTree b){
  return weight(a) < weight(b);
}

/**
 * Construye una PriorityQueue vacía.
 **/
PriorityQueue emptyPriorityQueue() {
	PriorityQueue priorityQueue = new PriorityQueueStr;
	priorityQueue->size = 0;
	priorityQueue->elements = new HuffmanTree[MAX_SIZE + 1];
	inicializarPriorityQueue(priorityQueue);
	return priorityQueue;
}

/**
 * Libera la memoria utilizada por la PriorityQueue.
 **/
void deletePriorityQueue(PriorityQueue& q) {
	delete[] q->elements;
	delete q;
}

/**
 * Retorna la cantidad de elementos en la PriorityQueue.
 * Complejidad: O(1)
 **/
int size(PriorityQueue q) {
	return q->size;
}

/**
 * Agrega un elemento a la PriorityQueue.
 * Complejidad: O(log(size(q)))
 **/
void enqueue(PriorityQueue& q, HuffmanTree t) {
	int i;

	for(
		i = ++(q->size);
		q->elements[i/2] != NULL && !tieneMayorPrioridad(q->elements[i/2], t);
		i = i/2
	){
		q->elements[i] = q->elements[i/2];
	}

	q->elements[i] = t;
}

/**
 * Remueve y retorna el elemento de menor prioridad de la PriorityQueue.
 * Complejidad: O(log(size(q)))
 **/
HuffmanTree dequeue(PriorityQueue& q) {
	HuffmanTree minElem = q->elements[1];
	HuffmanTree lastElem = q->elements[q->size--];

	int child = 0, i = 0;

	for (i = 1; i * 2 <= q->size; i = child) {
		child = i * 2;

		if (
      child != q->size 
      && tieneMayorPrioridad(q->elements[child + 1], q->elements[child])
    ) {
			child++;
		}

		if (!tieneMayorPrioridad(lastElem, q->elements[child])){
			q->elements[i] = q->elements[child];
		} else {
			break;
		}
	}

	q->elements[i] = lastElem;

	return minElem;
}
