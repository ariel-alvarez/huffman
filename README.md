# Compresor de Huffman #

Trabajo práctico de Estructuras de Datos - Licenciatura en Desarrollo de Software - UNQ

### Requerimientos ###

* g++ o algún compilador default para c++
* [CMake](http://www.cmake.org/) instalado


### Cómo correr los tests ###
Al ejecutar ./buildAndTest.sh (darle permisos de ejecución de ser necesario) se buildean todos los ejecutables, que quedarán en el directorio build, y se corren los tres tests (que se encuentran en el directorio /test).

Los tests se escribieron usando [cspec](https://github.com/pepita-remembrance/cspec), un framework desarrollado por Federico Scarpa.

### Autor ###
Ariel Alvarez
alvarez dot ariel dot work at gmail dot com
