#include <stdexcept>

#include "cspec.h"
#include "HuffmanTree.h"

int main(int argc, char **argv) {
	describe("Huffman Tree", function() {
		static HuffmanTree tree;

		after(function() {
			deleteHuffmanTree(tree);
		});

		it("cada hoja conoce su peso", function() {
			tree = leaf('h', 8);
			should_int(weight(tree)) be equal to(8);
		});

    it("el peso de un arbol de Huffman es la suma de los pesos de todas sus hojas (con 2 hojas)", function(){
      tree = binary(
                leaf('a', 1),
                leaf('b', 2)
             );
      should_int(weight(tree)) be equal to(3);
    });

    it("el peso de un arbol de Huffman es la suma de los pesos de todas sus hojas (con 3 hojas)", function(){
      tree = binary(
                  binary(
                      leaf('a', 1),
                      leaf('b', 2)
                  ),
                  leaf('c', 3)
             );
      should_int(weight(tree)) be equal to(6);
    });

		it("el peso de un arbol de Huffman es la suma de los pesos de todas sus hojas (con 4 hojas)", function(){
			tree = binary(
                  binary(
                      leaf('a', 1),
                      leaf('b', 2)
                  ),
                  binary(
                      leaf('c', 3),
                      leaf('d', 4)
                  )
             );
			should_int(weight(tree)) be equal to(10);
		});

	});
  return CSPEC_RESULT;
}
