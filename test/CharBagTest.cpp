#include <stdexcept>

#include "cspec.h"
#include "CharBag.h"


#define CHARBAG_SIZE 256

int main(int argc, char **argv) {

  describe("CharBag", function () {

    static CharBag charBag;

    describe("Contador de repeticiones", function() {

      before(function(){
        charBag = emptyCharBag(CHARBAG_SIZE);
      });

      it("Un caracter inexistente tiene 0 repeticiones", function() {
        should_int(get(charBag, 'a')) be equal to(0);
      });

      it("Un caracter guardado una vez tiene 1 repeticion", function() {
        add(charBag, 'a');
        should_int(get(charBag, 'a')) be equal to(1);
      });

      it("Varios caracteres guardados tienen sus correspondientes repeticiones", function() {
        add(charBag, 'a');
        add(charBag, 'd');
        add(charBag, 'a');
        add(charBag, 'a');
        add(charBag, 'c');
        add(charBag, 'h');

        should_int(get(charBag, 'a')) be equal to(3);
        should_int(get(charBag, 'd')) be equal to(1);
        should_int(get(charBag, 'z')) be equal to(0);
      });

      it("Un charbag puede contener todos los valores posibles (0..255)", function() {
        unsigned char i = 0 ;
        do {
          add(charBag, i);
        } while ( ++i ) ;

        i = 0 ;
        do {
          should_int(get(charBag, i)) be equal to(1);
        } while ( ++i ) ;

      });

      it("remueve la ocurrencia de un caracter que apareció dos veces, quedando la cantidad de ocurrencias = 1", function() {
        add(charBag, 'a');
        add(charBag, 'a');
        remove(charBag, 'a');
        should_int(get(charBag, 'a')) be equal to(1);
      });

      it("remueve la ocurrencia de un caracter que apareció una vez, quedando la cantidad de ocurrencias = 0", function() {
        add(charBag, 'a');

        remove(charBag, 'a');
        should_int(get(charBag, 'a')) be equal to(0);
      });

      it("debe mantener el invariante de representación al eliminar un caracter de una sóla ocurrencia", function() {
        add(charBag, 'a');
        add(charBag, 'b');

        remove(charBag, 'a');

        should_int(charBag->siguienteLibre) be equal to(1);
        should_int(get(charBag, 'a')) be equal to(0);
        should_int(get(charBag, 'b')) be equal to(1);
      });

    });

    describe("Iterator", function() {

      static CharBagIterator iterator;

      before(function(){
        charBag = emptyCharBag(CHARBAG_SIZE);
        iterator = iterate(charBag);
      });

      it("conoce el primer caracter al ser creado", function() {
        add(charBag, '?');
        should_char(currentChar(iterator)) be equal to('?');
      });

      it("conoce la cantidad de repeticiones del primer caracter", function() {
        add(charBag, 'x');
        add(charBag, 'x');

        should_int(currentCount(iterator)) be equal to(2);
      });

      it("entiende si el elemento actual es válido", function() {
        add(charBag, 'a');
        should_bool(valid(iterator)) be truthy;
      });

      it("entiende si el elemento actual es inválido", function() {
        add(charBag, 'a');
        next(iterator);

        should_bool(valid(iterator)) be falsey;
      });

      it("avanza al siguiente elemento al llamarse a next", function() {
        add(charBag, 'a');
        add(charBag, 'b');
        add(charBag, 'b');
        add(charBag, 'c');

        next(iterator);

        should_char(currentChar(iterator)) be equal to('b');
      });

      it("es válido si está completo y el último elemento es 255", function() {
        unsigned char i = 0 ;
        do {
          add(charBag, i);
        } while ( ++i ) ;

        i = 0 ;
        while ( ++i ){
          next(iterator);
        }

        i = 255;
        should_char(currentChar(iterator)) be equal to(i);
        should_bool(valid(iterator)) be truthy;
      });

      it("lanza una excepción si se intenta hacer next en un estado inválido", function() {
        try {
          next(iterator);
          should_bool(false) be truthy;
        } catch( const std::invalid_argument& e ) {
          should_bool(true) be truthy;
        }
      });

      it("remueve la ocurrencia de un caracter que apareció dos veces, quedando la cantidad de ocurrencias = 1", function() {
        add(charBag, 'a');
        add(charBag, 'a');
        removeCurrent(iterator);
        should_int(get(charBag, 'a')) be equal to(1);
      });

      it("remueve la ocurrencia de un caracter que apareció una vez, quedando la cantidad de ocurrencias = 0", function() {
        add(charBag, 'a');

        removeCurrent(iterator);
        should_int(get(charBag, 'a')) be equal to(0);
      });

      it("debe mantener el invariante de representación al eliminar un caracter de una sóla ocurrencia", function() {
        add(charBag, 'a');
        add(charBag, 'b');

        removeCurrent(iterator);

        should_int(charBag->siguienteLibre) be equal to(1);
        should_int(get(charBag, 'a')) be equal to(0);
        should_int(get(charBag, 'b')) be equal to(1);
      });

      it("debe mantener el invariante de representación al eliminar un caracter de una sóla ocurrencia que es el primero", function() {
        add(charBag, 'a');
        add(charBag, 'b');

        next(iterator);
        removeCurrent(iterator);

        should_int(charBag->siguienteLibre) be equal to(1);
        should_char(charBag->caracteres[charBag->siguienteLibre-1]) be equal to('a');
        should_int(get(charBag, 'a')) be equal to(1);
        should_int(get(charBag, 'b')) be equal to(0);
      });

    });

  });

  return CSPEC_RESULT;
}
