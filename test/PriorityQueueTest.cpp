#include "cspec.h"
#include "PriorityQueue.h"
#include "HuffmanTree.h"

int main(int argc, char **argv) {
	describe("PriorityQueue", function() {
		static PriorityQueue queue;

		before(function(){
			queue = emptyPriorityQueue();
		});

		after(function(){
			deletePriorityQueue(queue);
		});

		it("tiene size 0 si no tiene elementos", function() {
			should_int(size(queue)) be equal to(0);
		});

		it("tiene size N si tiene N elementos", function() {
			enqueue(queue, leaf('a', 3));
			enqueue(queue, leaf('b', 2));
			enqueue(queue, leaf('c', 7));

			should_int(size(queue)) be equal to(3);
		});

		it("dequeue los elementos de menor prioridad a mayor prioridad", function() {
			enqueue(queue, leaf('a', 3));
			enqueue(queue, leaf('b', 2));
			enqueue(queue, leaf('c', 4));
			enqueue(queue, leaf('d', 1));

			should_int(weight(dequeue(queue))) be equal to(1);
			should_int(weight(dequeue(queue))) be equal to(2);
			should_int(weight(dequeue(queue))) be equal to(3);
			should_int(weight(dequeue(queue))) be equal to(4);
		});

		it("puede guardar los 256 elementos que se van a usar", function() {
			unsigned char caracter = 0 ;
			unsigned char cantidad = 256;

			do {
				enqueue(queue, leaf(caracter, cantidad));
				cantidad--;
			} while ( ++caracter ) ;

			should_int(weight(dequeue(queue))) be equal to(cantidad);
		});


	});

  return CSPEC_RESULT;
}
